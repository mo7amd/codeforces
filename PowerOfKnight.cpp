#include <vector>
#include <iostream>
#include <sstream>
#include <string>

  using namespace std;

#define MAX 8
int Nx,Ny;
int Kx,Ky;
int Qx,Qy;
int Ox,Oy;
bool possible[MAX][MAX];
void moveKnight(int x, int y);
void findKing(int x, int y);
bool foundk = false;
bool foundq = false;
bool found = false;
int main()
{
  for(int i=0;i<MAX;i++)
    for(int j=0;j<MAX;j++)
      possible[i][j]=false;

  std::string chess_board[MAX];
  for(int i=(MAX-1);i>=0;i--){
    std::getline(cin,chess_board[i]);
  }

for(int i=0;i<MAX;i++){
  for(int j=0;j<MAX;j++){
    if(chess_board[i][j] == 'N')  {Nx=i;Ny=j;/*cout<<Nx<<" "<<Ny<<endl;*/}
    if(chess_board[i][j] == 'K')  {Kx=i;Ky=j;/*cout<<Kx<<" "<<Ky<<endl;*/}
    if(chess_board[i][j] == 'Q')  {Qx=i;Qy=j;/*cout<<Qx<<" "<<Qy<<endl;*/}
  }
}

moveKnight(Nx+2, Ny-1);
moveKnight(Nx+2, Ny+1);
moveKnight(Nx+1, Ny-2);
moveKnight(Nx+1, Ny+2);
moveKnight(Nx-1, Ny-2);
moveKnight(Nx-1, Ny+2);
moveKnight(Nx-2, Ny-1);
moveKnight(Nx-2, Ny+1);

  if(found!=true)  cout << "NO";
  return 0;
}
void moveKnight(int x, int y)
{
  if( x < 0 || x >= MAX || y < 0 || y >= MAX)
  {
      return;
  }

  Ox=x;
  Oy=y;
  foundk = false;
  foundq = false;

  findKing(x+2, y-1);
  findKing(x+2, y+1);
  findKing(x+1, y-2);
  findKing(x+1, y+2);
  findKing(x-1, y-2);
  findKing(x-1, y+2);
  findKing(x-2, y-1);
  findKing(x-2, y+1);
}
void findKing(int x, int y)
{
  if( x < 0 || x >= MAX || y < 0 || y >= MAX)
  {
      return;
  }

  else{

    if(x==Kx && y==Ky) foundk=true;
    if(x==Qx && y==Qy)  foundq=true;

    if(foundk == true && foundq == true && found == false){
      found = true;
      if(found){
      char x[8]={'A','B','C','D','E','F','G','H'};
      char y[8]={'1','2','3','4','5','6','7','8'};
      cout<< "YES"<<" "<<y[Ox]<<x[Oy];

      }
    }
  }
}
